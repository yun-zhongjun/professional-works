#ifndef __HEAD_H__
#define __HEAD_H__
#include <STC89C5xRC.H>
#include <intrins.h>
#define uint unsigned int
#define uchar unsigned char

#define LCD_DATA P1

sbit rs=P3^2;
sbit rw=P3^3;
sbit en=P3^4;
sbit psb=P3^5;

sbit K1=P2^0;
sbit K2=P0^7;
sbit K3=P0^6;
sbit K4=P0^5;
sbit K5=P0^4;
sbit K6=P0^3;
sbit K7=P0^2;
sbit K8=P0^1;
sbit K9=P0^0;

typedef struct pai
{
uchar name[12];
uchar chengji[9];

}pp;

void xshu(uchar date);
void xming(uchar com);
void yanshi(uint x);
void chushi();
void chinese_play(uchar *str,char y,char x);//只能显示汉字或者连续双字母
void game_stat(void);
void qing();
void kaishi();
void gamee(char game);
void readd();
void pai_show(char q);
void menu(void);
void paihang(char abc);
void writee();
void menu_o(char s);
#endif